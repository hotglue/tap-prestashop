"""prestashop tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_prestashop.streams import (
    OrdersStream,
    ProductsStream,
    StockAvailablesStream,
    CategoriesStream,
    SuppliersStream,
    CurrenciesStream,
    ShopsStream,
    CountriesStream,
    OrderCarriersStream,
    OrderDetailsStream,
    OrderHistoriesStream,
    OrderInvoicesStream,
    OrderPaymentsStream,
    OrderSlipStream,
    OrderStatesStream,
    CombinationsStream,
    ProductOptionValuesStream,
    AddressesStream,
    SupplyOrdersStream,
    SupplyOrderStatesStream,
    SupplyOrderDetailsStream,
    WarehousesStream,
    StocksStream,
)

STREAM_TYPES = [
    OrdersStream,
    ProductsStream,
    StockAvailablesStream,
    CategoriesStream,
    SuppliersStream,
    CurrenciesStream,
    ShopsStream,
    CountriesStream,
    OrderCarriersStream,
    OrderDetailsStream,
    OrderHistoriesStream,
    OrderInvoicesStream,
    OrderPaymentsStream,
    OrderSlipStream,
    OrderStatesStream,
    CombinationsStream,
    ProductOptionValuesStream,
    AddressesStream,
    SupplyOrdersStream,
    SupplyOrderStatesStream,
    SupplyOrderDetailsStream,
    WarehousesStream,
    StocksStream,
]


class Tapprestashop(Tap):
    """prestashop tap class."""
    name = "tap-prestashop"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "auth_token",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service"
        ),
        th.Property(
            "api_url",
            th.StringType,
            required=True,
            description="The url for the API service"
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            required=True,
            description="The earliest record date to sync"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__=="__main__":
    Tapprestashop.cli()