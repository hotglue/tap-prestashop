"""REST client handling, including prestashopStream base class."""

import requests
import logging
import string
import random
from pathlib import Path
from typing import Any, Callable, Dict, Optional, List, Iterable
from requests.exceptions import JSONDecodeError

import xmltodict
from xml.etree import ElementTree
from lxml import etree
import backoff
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from tap_prestashop.utils import extract_text, format_associations, extract_categories_text, clean_categories_data


SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
logging.getLogger("backoff").setLevel(logging.CRITICAL)

class prestashopStream(RESTStream):
    """prestashop stream class."""

    _page_size = 1000
    disable_full = ["supply_order_state"]
    stream_detail_type = ["supply_order_state"]
    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return f"{self.config['api_url']}/api"

    records_jsonpath = "$[*]"

    @property
    def authenticator(self) -> BasicAuthenticator:
        """Return a new authenticator object."""
        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config.get("auth_token"),
            password="",
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}

        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        else:
            headers["User-Agent"] = ''.join(random.choices(string.ascii_letters, k=8))
        if not self.config.get("xml_api"):
            headers["Output-Format"] = "JSON"
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        
        #Break the loop for detail type streams
        if self.name in self.stream_detail_type:
            return None
        if not previous_token:
            return self._page_size
        if self.config.get("xml_api"):
            if self.xml_to_dict(response) is None:
                return None
        else:
            if not response.json():
                return None

        return previous_token + self._page_size

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["ws_key"] = self.config.get("auth_token")
        if self.replication_key:
            params["sort"] = f"[{self.replication_key}_ASC]"
            params["date"] = 1
            
            start_date = self.get_starting_timestamp(context)
            date = start_date.strftime("%Y-%m-%d %H:%M:%S")
            params[f"filter[{self.replication_key}]"] = f">[{date}]"

        params["limit"] = f"{(next_page_token or 0)},{self._page_size}"
        params["display"] = "full"
        if self.name in self.disable_full:
            del params['display']
        return params
    
    def xml_to_dict(self, response):
        xml_response = response.content.decode().replace("\x10", "")
        parser = etree.XMLParser(recover=True)
        xml_response = ElementTree.tostring(etree.fromstring(xml_response.encode(), parser=parser))
        xml_dict = xmltodict.parse(xml_response)["prestashop"][self.name]
        return xml_dict

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        if self.config.get("xml_api"):
            xml_dict = self.xml_to_dict(response)
            if xml_dict is None:
                return None
            if self.name in self.stream_detail_type:
                data = extract_text(xml_dict)
            else:        
                data = list(xml_dict.values())[0]
                if self.name=="categories":
                    data = extract_categories_text(data)
                    if isinstance(data, dict):
                        data = [data]
                    data = clean_categories_data(data)
                else:    
                    data = extract_text(data)
            if isinstance(data, dict):
                data = [data]
            for d in data:
                if isinstance(d, list):
                    #Skip the weird list of lists
                    pass
                else:
                    yield format_associations(d)

        else:
            try:
                yield from extract_jsonpath(self.records_jsonpath, input=response.json())
            except JSONDecodeError :
                raise Exception(f"Invalid response from prestashop. Are you sure that {self.url_base} is the right API url?") 

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        row["id"] = int(row["id"])
        if self.replication_key:
            if not row.get(self.replication_key):
                row[self.replication_key] = "2000-01-01 00:00:00"
        if "manufacturer_name" in row:
            #Some tenants will have boolean value for this property
            row['manufacturer_name'] = str(row['manufacturer_name'])        
        return row

    def reconnect(self, details):
        self.logger.info(f"Backing off try {details['tries']}")
        self._requests_session = requests.Session()

    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures."""
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError
            ),
            on_backoff = self.reconnect,
            max_tries=5,
            factor=3,
        )(func)
        return decorator

    def validate_response(self, response: requests.Response) -> None:
        """Validate HTTP response."""
        if 400 <= response.status_code < 500:
            msg = (
                f"{response.status_code} Client Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise FatalAPIError(msg)

        elif 500 <= response.status_code < 600:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            if response.status_code == 500 and self.name =='products':
                try:
                    data = response.json()
                except:
                    raise RetriableAPIError(msg)
            else:    
                raise RetriableAPIError(msg)
        
        if self.config.get("xml_api"):
            self.xml_to_dict(response)
