"""Stream type classes for tap-prestashop."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th

from tap_prestashop.client import prestashopStream


SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class OrdersStream(prestashopStream):
    """Define orders stream."""

    name = "orders"
    path = "/orders"
    primary_keys = ["id"]
    records_jsonpath = "$.orders[*]"
    replication_key = "date_upd"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_address_delivery", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_address_invoice", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_cart", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_currency", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_lang", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_customer", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_carrier", th.CustomType({"type": ["array", "string"]})),
        th.Property("current_state", th.CustomType({"type": ["array", "string"]})),
        th.Property("module", th.CustomType({"type": ["array", "string"]})),
        th.Property("invoice_number", th.CustomType({"type": ["array", "string"]})),
        th.Property("invoice_date", th.DateTimeType),
        th.Property("delivery_number", th.CustomType({"type": ["array", "string"]})),
        th.Property("delivery_date", th.DateTimeType),
        th.Property("valid", th.CustomType({"type": ["array", "string"]})),
        th.Property("date_add", th.DateTimeType),
        th.Property("date_upd", th.DateTimeType),
        th.Property("shipping_number", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_shop_group", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_shop", th.CustomType({"type": ["array", "string"]})),
        th.Property("secure_key", th.CustomType({"type": ["array", "string"]})),
        th.Property("payment", th.CustomType({"type": ["array", "string"]})),
        th.Property("recyclable", th.CustomType({"type": ["array", "string"]})),
        th.Property("gift", th.CustomType({"type": ["array", "string"]})),
        th.Property("gift_message", th.CustomType({"type": ["array", "string"]})),
        th.Property("mobile_theme", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_discounts", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_discounts_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_discounts_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_paid", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_paid_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_paid_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_paid_real", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_products", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_products_wt", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_shipping", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_shipping_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_shipping_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("carrier_tax_rate", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_wrapping", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_wrapping_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_wrapping_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("round_mode", th.CustomType({"type": ["array", "string"]})),
        th.Property("round_type", th.CustomType({"type": ["array", "string"]})),
        th.Property("conversion_rate", th.CustomType({"type": ["array", "string"]})),
        th.Property("reference", th.CustomType({"type": ["array", "string"]})),
        th.Property(
            "associations",
            th.ObjectType(
                th.Property(
                    "order_rows",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("id", th.CustomType({"type": ["array", "string"]})),
                            th.Property("product_id", th.CustomType({"type": ["array", "string"]})),
                            th.Property("product_attribute_id", th.CustomType({"type": ["array", "string"]})),
                            th.Property("product_quantity", th.CustomType({"type": ["array", "string"]})),
                            th.Property("product_name", th.CustomType({"type": ["array", "string"]})),
                            th.Property("product_reference", th.CustomType({"type": ["array", "string"]})),
                            th.Property("product_ean13", th.CustomType({"type": ["array", "string"]})),
                            th.Property("product_isbn", th.CustomType({"type": ["array", "string"]})),
                            th.Property("product_upc", th.CustomType({"type": ["array", "string"]})),
                            th.Property("product_price", th.CustomType({"type": ["array", "string"]})),
                            th.Property("id_customization", th.CustomType({"type": ["array", "string"]})),
                            th.Property("unit_price_tax_incl", th.CustomType({"type": ["array", "string"]})),
                            th.Property("unit_price_tax_excl", th.CustomType({"type": ["array", "string"]})),
                        )
                    ),
                ),
            ),
        ),
    ).to_dict()


    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {"order_id": record["id"]}


class ProductsStream(prestashopStream):
    """Define products stream."""

    name = "products"
    path = "/products"
    primary_keys = ["id"]
    records_jsonpath = "$.products[*]"
    replication_key = "date_upd"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_manufacturer", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_supplier", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_category_default", th.CustomType({"type": ["array", "string"]})),
        th.Property("new", th.CustomType({"type": ["array", "string"]})),
        th.Property("cache_default_attribute", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_default_image", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_tax_rules_group", th.CustomType({"type": ["array", "string"]})),
        th.Property("position_in_category", th.CustomType({"type": ["array", "string"]})),
        th.Property("manufacturer_name", th.StringType),
        th.Property("quantity", th.CustomType({"type": ["array", "integer", "string"]})),
        th.Property("type", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_shop_default", th.CustomType({"type": ["array", "string"]})),
        th.Property("reference", th.CustomType({"type": ["array", "string"]})),
        th.Property("supplier_reference", th.CustomType({"type": ["array", "string"]})),
        th.Property("location", th.CustomType({"type": ["array", "string"]})),
        th.Property("width", th.CustomType({"type": ["array", "string"]})),
        th.Property("height", th.CustomType({"type": ["array", "string"]})),
        th.Property("depth", th.CustomType({"type": ["array", "string"]})),
        th.Property("weight", th.CustomType({"type": ["array", "string"]})),
        th.Property("quantity_discount", th.CustomType({"type": ["array", "string"]})),
        th.Property("ean13", th.CustomType({"type": ["array", "string"]})),
        th.Property("isbn", th.CustomType({"type": ["array", "string"]})),
        th.Property("upc", th.CustomType({"type": ["array", "string"]})),
        th.Property("cache_is_pack", th.CustomType({"type": ["array", "string"]})),
        th.Property("cache_has_attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("is_virtual", th.CustomType({"type": ["array", "string"]})),
        th.Property("state", th.CustomType({"type": ["array", "string"]})),
        th.Property("additional_delivery_times", th.CustomType({"type": ["array", "string"]})),
        th.Property("delivery_in_stock", th.CustomType({"type": ["array", "string", "object"]})),
        th.Property("delivery_out_stock", th.CustomType({"type": ["array", "string"]})),
        th.Property("on_sale", th.CustomType({"type": ["array", "string"]})),
        th.Property("online_only", th.CustomType({"type": ["array", "string"]})),
        th.Property("ecotax", th.CustomType({"type": ["array", "string"]})),
        th.Property("minimal_quantity", th.CustomType({"type": ["array", "string"]})),
        th.Property("low_stock_threshold", th.CustomType({"type": ["array", "string"]})),
        th.Property("low_stock_alert", th.CustomType({"type": ["array", "string"]})),
        th.Property("price", th.CustomType({"type": ["array", "string"]})),
        th.Property("wholesale_price", th.CustomType({"type": ["array", "string"]})),
        th.Property("unity", th.CustomType({"type": ["array", "string"]})),
        th.Property("unit_price_ratio", th.CustomType({"type": ["array", "string", "number"]})),
        th.Property("additional_shipping_cost", th.CustomType({"type": ["array", "string"]})),
        th.Property("customizable", th.CustomType({"type": ["array", "string"]})),
        th.Property("text_fields", th.CustomType({"type": ["array", "string"]})),
        th.Property("uploadable_files", th.CustomType({"type": ["array", "string"]})),
        th.Property("active", th.CustomType({"type": ["array", "string"]})),
        th.Property("redirect_type", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_type_redirected", th.CustomType({"type": ["array", "string"]})),
        th.Property("available_for_order", th.CustomType({"type": ["array", "string"]})),
        th.Property("available_date", th.DateTimeType),
        th.Property("show_condition", th.CustomType({"type": ["array", "string"]})),
        th.Property("condition", th.CustomType({"type": ["array", "string"]})),
        th.Property("show_price", th.CustomType({"type": ["array", "string"]})),
        th.Property("indexed", th.CustomType({"type": ["array", "string"]})),
        th.Property("visibility", th.CustomType({"type": ["array", "string"]})),
        th.Property("advanced_stock_management", th.CustomType({"type": ["array", "string"]})),
        th.Property("date_add", th.DateTimeType),
        th.Property("date_upd", th.DateTimeType),
        th.Property("pack_stock_type", th.CustomType({"type": ["array", "string"]})),
        th.Property("meta_description", th.CustomType({"type": ["array", "string"]})),
        th.Property("meta_keywords", th.CustomType({"type": ["array", "string"]})),
        th.Property("meta_title", th.CustomType({"type": ["array", "string"]})),
        th.Property("link_rewrite", th.CustomType({"type": ["array", "string"]})),
        th.Property("name", th.CustomType({"type": ["array", "string", "object"]})),
        th.Property("description", th.CustomType({"type": ["array", "string"]})),
        th.Property("description_short", th.CustomType({"type": ["array", "string"]})),
        th.Property("available_now", th.CustomType({"type": ["array", "string"]})),
        th.Property("available_later", th.CustomType({"type": ["array", "string"]})),
        th.Property(
            "associations",
            th.ObjectType(
                th.Property(
                    "categories",
                    th.ArrayType(th.ObjectType(th.Property("id", th.CustomType({"type": ["array", "string"]})))),
                ),
                th.Property(
                    "images",
                    th.ArrayType(th.ObjectType(th.Property("id", th.CustomType({"type": ["array", "string"]})))),
                ),
                th.Property("product_features", th.CustomType({"type": ["array", "string"]})),
                th.Property(
                    "tags",
                    th.ArrayType(th.ObjectType(th.Property("id", th.CustomType({"type": ["array", "string"]})))),
                ),
                th.Property(
                    "stock_availables",
                    th.ArrayType(th.ObjectType(th.Property("id", th.CustomType({"type": ["array", "string"]})))),
                ),
                th.Property(
                    "accessories",
                    th.ArrayType(th.ObjectType(th.Property("id", th.CustomType({"type": ["array", "string"]})))),
                ),
            ),
        ),
    ).to_dict()


class StockAvailablesStream(prestashopStream):
    """Define stock_availables stream."""

    name = "stock_availables"
    path = "/stock_availables"
    primary_keys = ["id"]
    records_jsonpath = "$.stock_availables[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_product", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_product_attribute", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_shop", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_shop_group", th.CustomType({"type": ["array", "string"]})),
        th.Property("quantity", th.CustomType({"type": ["array", "integer", "string"]})),
        th.Property("depends_on_stock", th.CustomType({"type": ["array", "string"]})),
        th.Property("out_of_stock", th.CustomType({"type": ["array", "string"]})),
        th.Property("location", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class CategoriesStream(prestashopStream):
    """Define categories stream."""

    name = "categories"
    path = "/categories"
    primary_keys = ["id"]
    records_jsonpath = "$.categories[*]"
    replication_key = "date_upd"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_parent", th.CustomType({"type": ["array", "string"]})),
        th.Property("level_depth", th.CustomType({"type": ["array", "string"]})),
        th.Property("active", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_shop_default", th.CustomType({"type": ["array", "string"]})),
        th.Property("is_root_category", th.CustomType({"type": ["array", "string"]})),
        th.Property("position", th.CustomType({"type": ["array", "string"]})),
        th.Property("date_add", th.DateTimeType),
        th.Property("date_upd", th.DateTimeType),
        th.Property("name", th.CustomType({"type": ["array", "string", "object"]})),
        th.Property("link_rewrite", th.CustomType({"type": ["array", "string"]})),
        th.Property("description", th.CustomType({"type": ["array", "string"]})),
        th.Property("meta_title", th.CustomType({"type": ["array", "string"]})),
        th.Property("meta_description", th.CustomType({"type": ["array", "string"]})),
        th.Property("meta_keywords", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class SuppliersStream(prestashopStream):
    """Define suppliers stream."""

    name = "suppliers"
    path = "/suppliers"
    primary_keys = ["id"]
    records_jsonpath = "$.suppliers[*]"
    replication_key = "date_upd"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("link_rewrite", th.CustomType({"type": ["array", "string"]})),
        th.Property("name", th.CustomType({"type": ["array", "string", "object"]})),
        th.Property("active", th.CustomType({"type": ["array", "string"]})),
        th.Property("date_add", th.DateTimeType),
        th.Property("date_upd", th.DateTimeType),
        th.Property("description", th.CustomType({"type": ["array", "string"]})),
        th.Property("meta_title", th.CustomType({"type": ["array", "string"]})),
        th.Property("meta_description", th.CustomType({"type": ["array", "string"]})),
        th.Property("meta_keywords", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class CurrenciesStream(prestashopStream):
    """Define currencies stream."""

    name = "currencies"
    path = "/currencies"
    primary_keys = ["id"]
    records_jsonpath = "$.currencies[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("iso_code", th.CustomType({"type": ["array", "string"]})),
        th.Property("numeric_iso_code", th.CustomType({"type": ["array", "string"]})),
        th.Property("precision", th.CustomType({"type": ["array", "string"]})),
        th.Property("conversion_rate", th.CustomType({"type": ["array", "string"]})),
        th.Property("deleted", th.CustomType({"type": ["array", "string"]})),
        th.Property("active", th.CustomType({"type": ["array", "string"]})),
        th.Property("name", th.CustomType({"type": ["array", "string", "object"]})),
        th.Property("symbol", th.CustomType({"type": ["array", "object", "string"]})),
    ).to_dict()


class ShopsStream(prestashopStream):
    """Define shops stream."""

    name = "shops"
    path = "/shops"
    primary_keys = ["id"]
    records_jsonpath = "$.shops[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_shop_group", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_category", th.CustomType({"type": ["array", "string"]})),
        th.Property("active", th.CustomType({"type": ["array", "string"]})),
        th.Property("deleted", th.CustomType({"type": ["array", "string"]})),
        th.Property("name", th.CustomType({"type": ["array", "string", "object"]})),
        th.Property("theme_name", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class CountriesStream(prestashopStream):
    """Define countries stream."""

    name = "countries"
    path = "/countries"
    primary_keys = ["id"]
    records_jsonpath = "$.countries[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_zone", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_currency", th.CustomType({"type": ["array", "string"]})),
        th.Property("call_prefix", th.CustomType({"type": ["array", "string"]})),
        th.Property("iso_code", th.CustomType({"type": ["array", "string"]})),
        th.Property("active", th.CustomType({"type": ["array", "string"]})),
        th.Property("contains_states", th.CustomType({"type": ["array", "string"]})),
        th.Property("need_identification_number", th.CustomType({"type": ["array", "string"]})),
        th.Property("need_zip_code", th.CustomType({"type": ["array", "string"]})),
        th.Property("zip_code_format", th.CustomType({"type": ["array", "string"]})),
        th.Property("display_tax_label", th.CustomType({"type": ["array", "string"]})),
        th.Property("name", th.CustomType({"type": ["array", "string", "object"]})),
    ).to_dict()


class OrderCarriersStream(prestashopStream):
    """Define countries stream."""

    name = "order_carriers"
    path = "/order_carriers"
    primary_keys = ["id"]
    records_jsonpath = "$.order_carriers[*]"
    replication_key = "date_add"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_order", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_carrier", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_order_invoice", th.CustomType({"type": ["array", "string"]})),
        th.Property("weight", th.CustomType({"type": ["array", "string"]})),
        th.Property("shipping_cost_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("shipping_cost_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("tracking_number", th.CustomType({"type": ["array", "string"]})),
        th.Property("date_add", th.DateTimeType),
    ).to_dict()


class OrderDetailsStream(prestashopStream):
    """Define order_details stream."""
    name = "order_details"

    parent_stream_type = OrdersStream

    path = "/order_details"
    primary_keys = ["id"]
    records_jsonpath = "$.order_details[*]"
    replication_key = None

    def get_url_params(self, context, next_page_token):

        params: dict = {}
        params['filter[id_order]'] = f"[{context['order_id']}]"
        params["ws_key"] = self.config.get("auth_token")

        params["limit"] = f"{(next_page_token or 0)},{self._page_size}"
        params["display"] = "full"
        if self.name in self.disable_full:
            del params['display']
        return params

    #path + "?filter[id]=[orderid]"

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_order", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_id", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_attribute_id", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_quantity_reinjected", th.CustomType({"type": ["array", "string"]})),
        th.Property("group_reduction", th.CustomType({"type": ["array", "string"]})),
        th.Property("discount_quantity_applied", th.CustomType({"type": ["array", "string"]})),
        th.Property("download_hash", th.CustomType({"type": ["array", "string"]})),
        th.Property("download_deadline", th.DateTimeType),
        th.Property("id_order_invoice", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_warehouse", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_shop", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_customization", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_name", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_quantity", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_quantity_in_stock", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_quantity_return", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_quantity_refunded", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_price", th.CustomType({"type": ["array", "string"]})),
        th.Property("reduction_percent", th.CustomType({"type": ["array", "string"]})),
        th.Property("reduction_amount", th.CustomType({"type": ["array", "string"]})),
        th.Property("reduction_amount_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("reduction_amount_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_quantity_discount", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_ean13", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_isbn", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_upc", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_reference", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_supplier_reference", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_weight", th.CustomType({"type": ["array", "string"]})),
        th.Property("tax_computation_method", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_tax_rules_group", th.CustomType({"type": ["array", "string"]})),
        th.Property("ecotax", th.CustomType({"type": ["array", "string"]})),
        th.Property("ecotax_tax_rate", th.CustomType({"type": ["array", "string"]})),
        th.Property("download_nb", th.CustomType({"type": ["array", "string"]})),
        th.Property("unit_price_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("unit_price_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_price_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_price_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_shipping_price_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_shipping_price_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("purchase_supplier_price", th.CustomType({"type": ["array", "string"]})),
        th.Property("original_product_price", th.CustomType({"type": ["array", "string"]})),
        th.Property("original_wholesale_price", th.CustomType({"type": ["array", "string"]})),
        th.Property(
            "associations",
            th.ObjectType(
                th.Property(
                    "taxes",
                    th.ArrayType(th.ObjectType(th.Property("id", th.CustomType({"type": ["array", "string"]})))),
                )
            ),
        ),
    ).to_dict()


class OrderHistoriesStream(prestashopStream):
    """Define order_histories stream."""

    name = "order_histories"
    path = "/order_histories"
    primary_keys = ["id"]
    records_jsonpath = "$.order_histories[*]"
    replication_key = "date_add"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_employee", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_order_state", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_order", th.CustomType({"type": ["array", "string"]})),
        th.Property("date_add", th.DateTimeType),
    ).to_dict()


class OrderInvoicesStream(prestashopStream):
    """Define order_invoices stream."""

    name = "order_invoices"
    path = "/order_invoices"
    primary_keys = ["id"]
    records_jsonpath = "$.order_invoices[*]"
    replication_key = "date_add"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_order", th.CustomType({"type": ["array", "string"]})),
        th.Property("number", th.CustomType({"type": ["array", "string"]})),
        th.Property("delivery_number", th.CustomType({"type": ["array", "string"]})),
        th.Property("delivery_date", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_discount_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_discount_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_paid_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_paid_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_products", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_products_wt", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_shipping_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_shipping_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("shipping_tax_computation_method", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_wrapping_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_wrapping_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("shop_address", th.CustomType({"type": ["array", "string"]})),
        th.Property("note", th.CustomType({"type": ["array", "string"]})),
        th.Property("date_add", th.DateTimeType),
    ).to_dict()


class OrderPaymentsStream(prestashopStream):
    """Define order_payments stream."""

    name = "order_payments"
    path = "/order_payments"
    primary_keys = ["id"]
    records_jsonpath = "$.order_payments[*]"
    replication_key = "date_add"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("order_reference", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_currency", th.CustomType({"type": ["array", "string"]})),
        th.Property("amount", th.CustomType({"type": ["array", "string"]})),
        th.Property("payment_method", th.CustomType({"type": ["array", "string"]})),
        th.Property("conversion_rate", th.CustomType({"type": ["array", "string"]})),
        th.Property("transaction_id", th.CustomType({"type": ["array", "string"]})),
        th.Property("card_number", th.CustomType({"type": ["array", "string"]})),
        th.Property("card_brand", th.CustomType({"type": ["array", "string"]})),
        th.Property("card_expiration", th.CustomType({"type": ["array", "string"]})),
        th.Property("card_holder", th.CustomType({"type": ["array", "string"]})),
        th.Property("date_add", th.DateTimeType),
    ).to_dict()


class OrderSlipStream(prestashopStream):
    """Define order_slip stream."""

    name = "order_slips"
    path = "/order_slip"
    primary_keys = ["id"]
    records_jsonpath = "$.order_slip[*]"
    replication_key = "date_upd"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_customer", th.CustomType({"type": ["array", "string"]})),
        th.Property("id_order", th.CustomType({"type": ["array", "string"]})),
        th.Property("conversion_rate", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_products_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_products_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_shipping_tax_excl", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_shipping_tax_incl", th.CustomType({"type": ["array", "string"]})),
        th.Property("amount", th.CustomType({"type": ["array", "string"]})),
        th.Property("shipping_cost", th.CustomType({"type": ["array", "string"]})),
        th.Property("shipping_cost_amount", th.CustomType({"type": ["array", "string"]})),
        th.Property("partial", th.CustomType({"type": ["array", "string"]})),
        th.Property("date_upd", th.DateTimeType),
        th.Property("date_add", th.DateTimeType),
    ).to_dict()


class OrderStatesStream(prestashopStream):
    """Define order_states stream."""

    name = "order_states"
    path = "/order_states"
    primary_keys = ["id"]
    records_jsonpath = "$.order_states[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("unremovable", th.CustomType({"type": ["array", "string"]})),
        th.Property("delivery", th.CustomType({"type": ["array", "string"]})),
        th.Property("hidden", th.CustomType({"type": ["array", "string"]})),
        th.Property("send_email", th.CustomType({"type": ["array", "string"]})),
        th.Property("module_name", th.CustomType({"type": ["array", "string"]})),
        th.Property("invoice", th.CustomType({"type": ["array", "string"]})),
        th.Property("color", th.CustomType({"type": ["array", "string"]})),
        th.Property("logable", th.CustomType({"type": ["array", "string"]})),
        th.Property("shipped", th.CustomType({"type": ["array", "string"]})),
        th.Property("paid", th.CustomType({"type": ["array", "string"]})),
        th.Property("pdf_delivery", th.CustomType({"type": ["array", "string"]})),
        th.Property("pdf_invoice", th.CustomType({"type": ["array", "string"]})),
        th.Property("deleted", th.CustomType({"type": ["array", "string"]})),
        th.Property("name", th.CustomType({"type": ["array", "string", "object"]})),
        th.Property("template", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class CombinationsStream(prestashopStream):
    """Define combinations stream."""
    name = "combinations"
    path = "/combinations"
    primary_keys = ["id"]
    records_jsonpath = "$.combinations[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_product", th.CustomType({"type": ["array", "string"]})),
        th.Property("location", th.CustomType({"type": ["array", "string"]})),
        th.Property("ean13", th.CustomType({"type": ["array", "string"]})),
        th.Property("isbn", th.CustomType({"type": ["array", "string"]})),
        th.Property("upc", th.CustomType({"type": ["array", "string"]})),
        th.Property("quantity", th.CustomType({"type": ["array", "integer", "string"]})),
        th.Property("reference", th.CustomType({"type": ["array", "string"]})),
        th.Property("supplier_reference", th.CustomType({"type": ["array", "string"]})),
        th.Property("wholesale_price", th.CustomType({"type": ["array", "string"]})),
        th.Property("price", th.CustomType({"type": ["array", "string"]})),
        th.Property("ecotax", th.CustomType({"type": ["array", "string"]})),
        th.Property("weight", th.CustomType({"type": ["array", "string"]})),
        th.Property("unit_price_impact", th.CustomType({"type": ["array", "string"]})),
        th.Property("minimal_quantity", th.CustomType({"type": ["array", "string"]})),
        th.Property("low_stock_threshold", th.CustomType({"type": ["array", "string"]})),
        th.Property("low_stock_alert", th.CustomType({"type": ["array", "string"]})),
        th.Property("default_on", th.CustomType({"type": ["array", "string"]})),
        th.Property("available_date", th.CustomType({"type": ["array", "string"]})),
        th.Property("associations", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()


class ProductOptionValuesStream(prestashopStream):
    """Define product_option_values stream."""
    name = "product_option_values"
    path = "/product_option_values"
    primary_keys = ["id"]
    records_jsonpath = "$.product_option_values[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_attribute_group", th.CustomType({"type": ["array", "string"]})),
        th.Property("color", th.CustomType({"type": ["array", "string"]})),
        th.Property("position", th.CustomType({"type": ["array", "string"]})),
        th.Property("name", th.CustomType({"type": ["array", "string", "object"]})),
    ).to_dict()


class AddressesStream(prestashopStream):
    """Define addresses stream."""

    name = "addresses"
    path = "/addresses"
    primary_keys = ["id"]
    records_jsonpath = "$.addresses[*]"
    replication_key = "date_upd"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("id_customer", th.StringType),
        th.Property("id_manufacturer", th.StringType),
        th.Property("id_supplier", th.StringType),
        th.Property("id_warehouse", th.StringType),
        th.Property("id_country", th.StringType),
        th.Property("id_state", th.StringType),
        th.Property("alias", th.StringType),
        th.Property("company", th.StringType),
        th.Property("lastname", th.StringType),
        th.Property("firstname", th.StringType),
        th.Property("vat_number", th.StringType),
        th.Property("address1", th.StringType),
        th.Property("address2", th.StringType),
        th.Property("postcode", th.StringType),
        th.Property("city", th.StringType),
        th.Property("other", th.StringType),
        th.Property("phone", th.StringType),
        th.Property("phone_mobile", th.StringType),
        th.Property("dni", th.StringType),
        th.Property("deleted", th.StringType),
        th.Property("date_add", th.DateTimeType),
        th.Property("date_upd", th.DateTimeType)
    ).to_dict()

class SupplyOrdersStream(prestashopStream):
    """Define product_option_values stream."""
    name = "supply_orders"
    path = "/supply_orders"
    primary_keys = ["id"]
    records_jsonpath = "$.supply_orders[*]"
    replication_key = "date_upd"
    schema = th.PropertiesList(
        th.Property("id", th.CustomType({"type": ["number", "string"]})),
        th.Property("id_supplier", th.StringType),
        th.Property("id_lang", th.StringType),
        th.Property("id_warehouse", th.StringType),
        th.Property("id_supply_order_state", th.StringType),
        th.Property("id_currency", th.StringType),
        th.Property("supplier_name", th.StringType),
        th.Property("reference", th.StringType),
        th.Property("date_delivery_expected", th.DateTimeType),
        th.Property("total_te", th.StringType),
        th.Property("total_with_discount_te", th.StringType),
        th.Property("total_ti", th.StringType),
        th.Property("total_tax", th.StringType),
        th.Property("discount_rate", th.StringType),
        th.Property("discount_value_te", th.StringType),
        th.Property("is_template", th.BooleanType),
        th.Property("date_add", th.DateTimeType),
        th.Property("date_upd", th.DateTimeType),
        th.Property("associations", th.CustomType({"type": ["object", "array"]})),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "id_supply_order_state": record["id_supply_order_state"],
            "id_supply_order":record["id"]
        }
class SupplyOrderStatesStream(prestashopStream):
    """Define product_option_values stream."""
    name = "supply_order_state"
    path = "/supply_order_states/{id_supply_order_state}"
    primary_keys = None
    records_jsonpath = "$.supply_order_states[*]"
    replication_key = None
    parent_stream_type = SupplyOrdersStream
    schema = th.PropertiesList(
        th.Property("isGenericName", th.StringType),
        th.Property("id_supply_order", th.CustomType({"type": ["number", "string"]})),
        th.Property("delivery_note", th.BooleanType),
        th.Property("editable", th.BooleanType),
        th.Property("receipt_state", th.BooleanType),
        th.Property("pending_receipt", th.BooleanType),
        th.Property("enclosed", th.BooleanType),
        th.Property("color", th.BooleanType),
        th.Property("name", th.CustomType({"type": ["object", "array", "object"]})),

    ).to_dict()
    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        row["id"] = int(row["id"])
        if self.replication_key:
            if not row.get(self.replication_key):
                row[self.replication_key] = "2000-01-01 00:00:00"
        row["id_supply_order"] = context.get("id_supply_order")
        return row
class SupplyOrderDetailsStream(prestashopStream):
    """Define product_option_values stream."""
    name = "supply_order_details"
    path = "/supply_order_details"
    primary_keys = None
    records_jsonpath = "$.supply_order_details[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("supply_order", th.StringType),
        th.Property("id_product", th.StringType),
        th.Property("id_product_attribute", th.StringType),
        th.Property("reference", th.StringType),
        th.Property("supplier_reference", th.StringType),
        th.Property("name", th.StringType),
        th.Property("ean13", th.StringType),
        th.Property("isbn", th.StringType),
        th.Property("upc", th.StringType),
        th.Property("mpn", th.StringType),
        th.Property("exchange_rate", th.StringType),
        th.Property("unit_price_te", th.StringType),
        th.Property("quantity_expected", th.StringType),
        th.Property("price_te", th.StringType),
        th.Property("discount_rate", th.StringType),
        th.Property("discount_value_te", th.StringType),
        th.Property("price_with_discount_te", th.StringType),
        th.Property("tax_rate", th.StringType),
        th.Property("tax_value", th.StringType),
        th.Property("price_ti", th.StringType),
        th.Property("tax_value_with_order_discount", th.StringType),
        th.Property("price_with_order_discount_te", th.StringType),
        th.Property("associations", th.CustomType({"type": ["object", "array"]})),

    ).to_dict()

class WarehousesStream(prestashopStream):
    name = "warehouses"
    path = "/warehouses"
    primary_keys = None
    records_jsonpath = "$.warehouses[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("id_address", th.StringType),
        th.Property("id_employee", th.StringType),
        th.Property("id_currency", th.StringType),
        th.Property("valuation", th.StringType),
        th.Property("deleted", th.BooleanType),
        th.Property("reference", th.StringType),
        th.Property("name", th.StringType),
        th.Property("management_type", th.StringType),
        th.Property("management_type", th.StringType),
        th.Property("associations", th.CustomType({"type": ["object", "array"]})),
    ).to_dict()
class StocksStream(prestashopStream):
    name = "stocks"
    path = "/stocks"
    primary_keys = None
    records_jsonpath = "$.stocks[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("id_warehouse", th.StringType),
        th.Property("id_product", th.StringType),
        th.Property("id_product_attribute", th.StringType),
        th.Property("real_quantity", th.StringType),
        th.Property("reference", th.StringType),
        th.Property("ean13", th.StringType),
        th.Property("upc", th.StringType),
        th.Property("physical_quantity", th.StringType),
        th.Property("usable_quantity", th.StringType),
        th.Property("price_te", th.StringType),
    ).to_dict()