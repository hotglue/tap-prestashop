def extract_text(input_data):
    if isinstance(input_data, list):
        output = [extract_text(data) for data in input_data]
    elif isinstance(input_data, dict):
        output = dict()
        for key, value in input_data.items():
            if key=="@id":
                key = "id"
            if key.startswith("@"):
                pass
            elif isinstance(value, dict):
                if "#text" in value.keys():
                    output[key] = value["#text"]
                else:
                    output[key] = extract_text(value)
            else:
                output[key] = extract_text(value)
    else:
        output = input_data
    if output=={} or output==None:
        return ""
    elif isinstance(output, dict):
        if "language" in output.keys():
            output = output["language"]
    return output

def extract_categories_text(input_data):
    if isinstance(input_data, list):
        return [extract_categories_text(item) for item in input_data]
    elif isinstance(input_data, dict):
        if '#text' in input_data:
            return input_data['#text']
        else:
            return {key: extract_categories_text(value) for key, value in input_data.items() if not key.startswith('@')}
    else:
        if isinstance(input_data, dict):
            if "language" in input_data.keys():
                return input_data["language"]
        return input_data

def clean_categories_data(data):
    for item in data:
        for key, value in item.items():
            item[key] = value
            if isinstance(value, dict):
                if "language" in value.keys():
                    item[key] = value["language"]
                        
    return data    


def format_associations(data):
    output = {}
    associations = data.get("associations")
    if associations is None:
        return data
    for key, value in associations.items():

        if isinstance(value, dict) and value:
            dict_key = next(iter(value), None)
            if dict_key is not None:
                dict_result = value[dict_key]
                if isinstance(dict_result, dict):
                    output[key] = [dict_result]
                else:
                    output[key] = dict_result          
    data["associations"] = output
    return data
